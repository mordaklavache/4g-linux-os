#!/bin/bash
set -e
echo "- Raspbian image extraction."
# --------------------------------------------------------------
### Image splited with this command:
### cat 2023-02-21-raspios-bullseye-armhf-lite.img.xz | split --bytes=$((1024 * 1024 * 64)) - 2023-02-21-raspios-bullseye-armhf-lite.img.xz.
cat 2023-02-21-raspios-bullseye-armhf-lite.img.xz.* > 2023-02-21-raspios-bullseye-armhf-lite.img.xz
unxz -vkf 2023-02-21-raspios-bullseye-armhf-lite.img.xz
# --------------------------------------------------------------
echo "- Copying to device: $1."
cp -v 2023-02-21-raspios-bullseye-armhf-lite.img $1
sync
# --------------------------------------------------------------
echo "- Applying bootfs modifications."
mount -v ${1}1 /mnt
cat >> /mnt/config.txt << EOF
dtoverlay=dwc2
EOF
cat /mnt/config.txt
sed -e 's/$/ modules-load=dwc2,g_ether/' -i /mnt/cmdline.txt
cat /mnt/cmdline.txt
echo "- Adding files to bootfs."
rsync -vrlptD bootfs/ /mnt/
sync
umount -v /mnt
# --------------------------------------------------------------
echo "- Applying rootfs modifications."
mount -v ${1}2 /mnt
cat >> /mnt/etc/network/interfaces << EOF

auto usb0
iface usb0 inet static
    address 192.168.10.1/24
    # gateway 192.168.10.1

auto wlan0
iface wlan0 inet static
    address 192.168.1.1/24
    # gateway 192.168.1.1
EOF
cat /mnt/etc/network/interfaces
echo "- Adding files to rootfs."
rsync -vrlptD sysrootfs/ /mnt/
chown -R 1000:1000 /mnt/home/pi
sync
umount -v /mnt
exit 0

# next: just enter command: `apt install hostapd` + `apt install isc-dhcp-server`
