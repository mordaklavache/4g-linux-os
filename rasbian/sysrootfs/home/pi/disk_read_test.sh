#!/bin/bash
a=0
while true
do
	echo "Initialisation du test de lecture:";
	dd if=$1 of=/dev/null bs=512 count=2000000 skip=$(($a*2000000))
	if [ $? -ne 0 ]; then
		echo "Read Error on "$((($a+1)*2000000*512))" octets:"$(($a));
		exit 1;
	fi
	echo "Lecture aux premiers "$((($a+1)*2000000*512))" octets effectuée:"$(($a));
	a=$((++a))
done
exit 0
